\begin{figure*}
    \centering
    \includegraphics[height = 0.35\textwidth]{figures/poisson-ratio-measurements.pdf}
    \includegraphics[height = 0.35\textwidth]{figures/load-unload}
    \caption{\emph{left:} uniaxial stretch testing setup with initial lengths $l_i$ and displacements $d_i$ (note that the displacements $d_i$ can be negative as is the case for $d_2$), \emph{right:} the stress-strain curve of a fabric sample forms a cycle because the followed path is not the same between loading and unloading. }
    \label{fig:uniaxial-setup}
\end{figure*}

\section{Measurements and fitting}
\label{sec:material-measurements}
We now describe experiments we conducted to calibrate the simulation model presented above. We conducted these experiments on an elastic textile and a flexible plastic filament commonly used for 3D printing on stretched fabric. The textile is a finely-knitted spandex fabric composed of 80\% polyamide and 20\% elastane, while the printing filament material is TPU 95A (thermoplastic polyurethane with a 95A shore hardness).

The parameters of the membrane energy $W_{\mathrm{membrane}}$ depend on $\alpha_1, \alpha_2, \alpha_3, \beta_1, \beta_2, \gamma_1, \gamma_2$ and $\nu_f$. We fit the stiffness coefficients $\alpha_i$, $\beta_i$ and $\gamma_i$, $i=1,2$, using the stress-strain curves obtained from uniaxial tensile tests (see Section \ref{sec:uniaxial_test}). 
We also use these tests to estimate the fabric's Poisson's ratio $\nu_f$ using video data acquired during the tests.
We obtain the shear modulus $\alpha_3$ from the shear tests described in Section \ref{sec:shear_test}.

The bending energy $W_{\mathrm{bending}}$ of the textile depends on the bending stiffness coefficient $k_B$. We fit this parameter using the cantilever tests described in Section \ref{sec:bending-tests}.

\subsection{Uniaxial stretch}
\label{sec:uniaxial_test}
The uniaxial tensile test consists in stretching a sample of material along a prescribed direction and recording the resulting stress as a function of the strain.
We prepared material samples in a standard \emph{dogbone} shape. The plastic was 3D printed into a 15 cm long by 1 cm wide shape with a similar printing orientation as the printed-on-fabric curves, while several fabric samples were laser cut out of a 11 \si{cm} long by 2 \si{cm} wide template. The fabric samples were cut out at different orientations to measure the orthotropy of the material. We measured the tensile response of samples oriented at, respectively, 0, 15, 30, 45, 60, 75, and \ang{90} with respect to direction $\be_1$ (see Fig. \ref{fig:fabric-axes}).

Fig.~\ref{fig:uniaxial-setup} (left) shows the rig setup we used to test our material samples. The tensile measurements were performed by an Instron 5865 machine with a $\SI{50}{N}$ force sensor which tracks both the displacement $d_1$ of the clamped endpoints and the applied force  $f$ (called the \emph{response} of the material). While performing these tests, we also record the material deformation \emph{transverse} to the sample by filming the deformation and measuring the width of the sample at each frame. This data is used to compute the average Poisson's ratio of the spandex material. 

The testing machine stretches and releases the samples by performing load-unload cycles, which creates loops characteristic of a hysteresis behavior when plotting the stress-strain curves, as shown in Fig.~\ref{fig:uniaxial-setup} (right). This path-dependent behavior is likely caused by internal friction between fibers of the fabric which rearrange as the textile gets stretched \citep{Miguel2013}. Since we want to model the behavior of the textile once it has been stretched and gets released, we are interested in the \emph{unloading} part of the curve (green in Fig.~\ref{fig:uniaxial-setup}).

% \melina{I'm rearranging the text a bit, so that we describe how to fit the parameters in the order we need them to be fitted.}

\paragraph{Estimation of the Poisson's ratios $\nu_f$ and $\nu_b$}
We start by estimating the fabric's average Poisson's ratio $\nu_f$, that will be needed to fit the other material parameters. Following \citet{Volino2009}, we compute the strains $E_{ii}$ with $i = 1,2$ (which correspond to the axial and transverse directions respectively) the rest lengths $l_i$ and the displacements $d_i$ (see Fig.~\ref{fig:uniaxial-setup} (left)) as
\begin{equation}
    E_{ii} = \frac{d_i} {l_i} + \frac{d_i^2}{2l_i^2}.
    \label{eq:ForceDisplacement2StressStrain}
\end{equation}
% Computing the axial and transverse strains from the measurements, respectively $E_{11}$ and $E_{22}$, 
We approximate the Poisson's ratio for one orientation by averaging all the $-\frac{E_{22}}{E_{11}}$ values over the course of the deformation. 
% \melina{Be careful, index i is defined wrt material axes, not swatch orientations. We also need to move the relation between displacement and strain (eq. \ref{eq:ForceDisplacement2StressStrain} here)} 
We then compute the final Poisson's ratio $\nu_f$ to be used for the fabric's material model as the average over all sampled orientations. This value is estimated to be about 0.3.

Our bilayer model assumes the two monolayers it is composed of are made of homogeneous materials with same Poisson's ratio $\nu_b$. Since this is not the case here, we define $\nu_b$ as the average of the Poisson's ratios of the two layers: for the fabric, we use the average Poisson's ratio $\nu_f$ obtained above;  for the plastic we use a value of 0.5 as the material used (thermoplastic polyurethane) is known to be almost incompressible \citep{Qi2005}. Therefore, $\nu_b$ is set to 0.4.

%\adrien{This a copy/paste from the previous section. Needs to be integrated in the main text.}
%Equation~\ref{eq:EBL2} gives the elastic energy of a bilayer composed of homogeneous materials with the same Poisson's ratio $\nu_b$. In our case, we use the average value of the Poisson's ratios of the two layers: the fabric's Poisson's ratio $\nu_f$ is measured from videos of a uniaxial tensile test as the ratio of transverse contraction to axial strain \melina{Give more details. This value is not unique, i.e. depends on the strain} and is estimated to be about 0.3; the plastic material used (thermoplastic polyurethane) is known to be almost incompressible \citep{Qi2005} which means a Poisson's ratio $\nu_p \approx 0.5$. Therefore, $\nu_b$ is set to 0.4.
%
%\melina{we need to say more about our assumptions regarding $\nu$. Are we assuming it to be constant? ($-\frac{\epsilon_{22}}{\epsilon_{11}}$ is not) How do we get the value?}

\paragraph{Estimation of the fabric's membrane stiffness parameters $\alpha_1$, $\alpha_2$, $\beta_1$, $\beta_2$, $\gamma_1$ and $\gamma_2$}
In Section \ref{sec:material_model}, we defined the stress-strain relation of the spandex material in terms of the entries of the second Piola-Kirchhoff stress tensor $\bS$ and the non-linear Green strain tensor $\bE$, better suited to model large deformations than the more conventional Cauchy stress and Cauchy strain tensors (see Equation~\ref{eq:stress_strain_formula}). More specifically, letting $S_{ii}$ and $E_{ii}$ denote the normal components associated to the direction $\be_i$ of, respectively, the stress tensor $\bS$ and the strain tensor $\bE$, the relations between $S_1$, $S_2$, $E_{11}$ and $E_{22}$ read
\begin{align}
   S_{11} &= \frac{1}{1 - {\nu_f}^2} \left( \alpha_1 E_{11} + \sqrt{\alpha_1 \alpha_2} {\nu_f} E_{22}\right) - \beta_1 \log \left(\frac{\gamma_1-E_{11}}{\gamma_1} \right) \\
    S_{22} &= \frac{1}{1 - {\nu_f}^2} \left( \alpha_2 E_{22} + \sqrt{\alpha_1 \alpha_2} {\nu_f}E_{11}\right)  - \beta_2 \log \left(\frac{\gamma_2-E_{22}}{\gamma_2} \right).
\end{align}
To fully decouple the stress-strain response along each material axis, and, in doing so, ease fitting of the stiffness parameters, we assume that the current transverse to axial strain ratio $-\frac{E_{ii}}{E_{jj}}$, $i\neq j$, can be approximated by the average Poisson's ratio $\nu_f$. This allows us to write the relation between $S_{ii}$ and $E_{ii}$ as
\begin{equation}
   S_{ii} = \tilde\alpha_i  E_{ii}- \beta_i \log \left(\frac{\gamma_i-E_{ii}}{\gamma_i} \right),  \ \ \text{with  }\tilde\alpha_i = \alpha_i  - \sqrt{\alpha_1 \alpha_2} {\nu_f}^2, \ \  i=1,2.
\end{equation}
After converting the measured force-displacement pairs on the \ang{0} and \ang{90} swatches into stress-strain data points using relations provided by \citet{Volino2009}, we fit the $a_i$, $\beta_i$ and $\gamma_i$ parameters to the stress-strain data using non-linear least squares.
%We convert force-displacement data points into entries of the second Piola-Kirchhoff stress tensor $\boldsymbol{\sigma} = \frac{\partial \Psi}{\partial \boldsymbol{\varepsilon}}$ and the Green strain tensor $\boldsymbol{\varepsilon}$ which are accurate quantities for large strains. For uniaxial stretch, the diagonal entries of these $2\times2$ matrices can be computed from the force-displacement curves of either the \ang{0} or \ang{90} samples:
% \begin{equation}
%     \begin{aligned}
%         E_{ii} &= \frac d L + \frac{d^2}{2L^2}\\
%         S_{ii} &= \frac 1 w \left( f  \frac{L}{d + L} \right).
%         \label{eq:ForceDisplacement2StressStrain}
%     \end{aligned}
% \end{equation}
%the $i=1$ values can be computed from the \ang{0} sample and the $i=2$ values can be computed from the \ang{90} one.
%the resulting force-displacement curves to \emph{stress-strain} curves to extract the parameters we are interested in. 
% \melina{Should we provide more details on what algo we used for fitting? Matlab?}
% \melina{Introduce $L$ and $w$ at the beginning of this section, when we mention the sizes of the samples.}
Finally, we compute $\alpha_1$ and $\alpha_2$ from $a_1$ and $a_2$ by solving the $2\times2$ system of equations. 
% \melina{Provide more details on the algo used for solving the system?}

%We use the uniaxial stretch data to find the coefficients of the fabric material model. We can compute the stress tensor by differentiating Equation~\ref{eq:fabric} (assuming $\varepsilon_{ii} > 0$):
%\begin{equation}
%    \boldsymbol{\sigma} = \frac{\partial \Psi}{\partial \boldsymbol{\varepsilon}} = \frac{1}{1- \nu^2} \begin{pmatrix} \alpha_1 & \sqrt{\alpha_1 \alpha_2} \nu & 0 \\  \sqrt{\alpha_1 \alpha_2} \nu & \alpha_2 & 0 \\ 0 & 0 & \alpha_3 (1 - \nu^2) \end{pmatrix} \begin{pmatrix} \varepsilon_{11} \\ \varepsilon_{22} \\ 2\varepsilon_{12} \end{pmatrix} - 
%    \begin{pmatrix} \beta_1 \log \left(\frac{\varepsilon_{11} - \gamma_1}{\gamma_1} \right) \\ \beta_2 \log \left(\frac{\varepsilon_{22} - \gamma_2}{\gamma_2} \right)\\ 0 \end{pmatrix}.
%\end{equation}
%We can use the relationship $\nu = -\frac{\varepsilon_{22}}{\varepsilon_{11}}$ to express $\sigma_{11}$ as function of $\varepsilon_{11}$, and $\sigma_{22}$ as function of $\varepsilon_{22}$:
%\begin{align}
%    \sigma_{11} &= \frac{1}{1 - \nu^2} \left( \alpha_1 - \sqrt{\alpha_1 \alpha_2} \nu^2 \right) \varepsilon_{11} - \beta_1 \log \left(\frac{\varepsilon_{11} - \gamma_1}{\gamma_1} \right) \\
%    \sigma_{22} &= \frac{1}{1 - \nu^2} \left( \alpha_2 - \sqrt{\alpha_1 \alpha_2} \nu^2 \right) \varepsilon_{22} - \beta_2 \log \left(\frac{\varepsilon_{22} - \gamma_2}{\gamma_2} \right).
%\end{align}
%We then fit a function of the form $\sigma_{ii}(\varepsilon) = a \varepsilon_{ii} - b \log\left(\frac{\varepsilon_{ii} - c}{c}\right)$ which allows to directly retrieve $\beta_1$, $\beta_2$, $\gamma_1$ and $\gamma_2$. 
%For $\alpha_1$ and $\alpha_2$, solving a simple nonlinear system allows to compute them as a function of $\nu$. 

Note that all these parameters depend on the stretching factor $s$ of the fabric. In practice we fit them using fabric swatches stretched up to 45\% and 70\% of their initial lengths, and use linearly interpolated values when we need to model the fabric for different pre-stretching rates. 
%These parameters also depend on how much was the fabric stretched, we can also linearly interpolate the fitted values for different amounts of stretch as was done for $E_2$. 

\paragraph{Estimation of the fabric's and plastic's Young's moduli $E_f$ and $E_p$}

\begin{figure}
    \centering
    \includegraphics[width = \linewidth]{figures/bilayer-fitting}
    \caption{\emph{left:} stress-strain curve for the plastic material, the slope of its tangent at 0 is the material's Young's modulus; \emph{right:} stress-strain curve for the fabric material, here we are interested in the slope of the tangent upon unloading.}
    \label{fig:moduli-fitting}
\end{figure}
% (measured from the horizontal axis in Fig.~\ref{fig:microscope-view})

% A common way to do this is by computing so-called \emph{engineering} stress and strains: the engineering stress is just the ratio of the force $f$ against the cross-section area $w\tau$ (where $w$ is the width and $\tau$ the thickness of the material), and the engineering strain is defined as the ratio of the displacement $d$ and the initial length $L$. Unfortunately, this method is only valid for small strains where all definitions of stress or strain converge, it can be used to measure the Young's modulus of a material -- which is the tangent of the stress-strain curve at zero -- but deviates from true values when considering larger strains. 

We use the stress-strain curves of the plastic and the different fabric samples to calibrate the bilayer material model (Equation~\ref{eq:EBL2}), and in particular, to measure the Young's moduli $E_f$ and $E_p$ of the two layers. 
To measure the Young's modulus $E_p$ of the plastic, we find the tangent at 0 of the stress-strain curve (Fig.~\ref{fig:moduli-fitting}, left), the slope of that tangent being the value of $E_p$. 3D printed objects are generally anisotropic and their response may vary depending on the orientation of the toolpaths used to fabricate them. We make sure that the toolpaths used for the measured sample are parallel to its main axis as it is the case for typical curves printed on fabric. 

To estimate the Young's modulus of the fabric substrate $E_f$, we follow a different procedure. Since the fabric material is pre-stretched to high strains when the plastic-fabric bilayer is formed, we are interested in its response for the full range of deformations, not just for strains close to zero. In fact, because the fabric is bounded by the plastic layer, it might never reach its original length. We account for such large deformations by computing the slope of a straight line approximating the full unloading part of the stress-strain curve (Fig.~\ref{fig:moduli-fitting}, right) for each sample orientation. 
The final value of $E_f$ is thus computed as the average of all the slopes for each sample orientation. 

% \melina{You need to explain how you get $E_f$ from the 7 curves you get. You said above that you averaged the values. This should be mentioned here.It would also be interesting to see how much anisotropy there is by showing the different curves.} \melina{Also, what do you do for the plastic? Do you assume it is isotropic? If you consider a single orientation, I guess it would make sense to stretch the material in the direction of the filaments. Is it what you do?}
% Classically, the Young's modulus is defined as the slope of the tangent at 0 of the stress-strain curve, it is a linear approximation of the stress-strain behavior for small strains, we can therefore compute the plastic's Young's modulus $E_p$ by finding the tangent at zero of its stress-strain curve (Fig.~\ref{fig:moduli-fitting}, left). For the fabric substrate however the situation is a bit different, since it was pre-stretched when the plastic-fabric bilayer was formed, it does not make much sense to consider an approximation of its response for strains close to zero since the plastic layer prevents it from ever going back to its original length. Instead, the linear approximation should start from the maximum strain value, $E_f$ is therefore computed as a pseudo ``Young's modulus''
% \note{(I don't know how to call it, it's like a Young's modulus, and it is used as such in the bilayer formula but it does not really fit the classical definition of the Young's modulus...)} 
% by finding the slope of the tangent at the point of release (Fig.~\ref{fig:moduli-fitting}, right) 
% \melina{I think the line should rather pass through the origin and the point of interest if you want to linearize the material behavior around the pre-stretched value of interest. See Bickel's work on modeling non-linear materials \cite{Bickel2009}}. \melina{To clarify. Text mentions the tangent at point of release, Fig.~\ref{fig:moduli-fitting}, right mentions an average, and I talk about the slope of the arc between 0 and the point of release.}
% Since this value is measured at the point of unloading, it may depend on how far the sample was stretched, to account for a dependency on the initial strain of the fabric, we performed two tests in which the fabric samples were stretched up to \SI{45}{\percent} and \SI{70}{\percent} of their initial lengths, and linearly interpolate between the two measured values to simulate self-shaping textiles pre-stretched with a different strain value.\melina{was this interpolation scheme actually tested? how much do we actually stretch the fabric in the examples we show here? what is the error we make when interpolating between the two values and directly fitting the Young's modulus for the fabric stretched by the desired amount directly?}\david{ We can remove this paragraph, what I meant was that it's possible to interpolate between  \SI{45}{\percent} and \SI{70}{\percent} since I have the two corresponding datasets, but in practice I only ran simulations for the \SI{70}{\percent} case. Maybe this can be mentioned for future work instead?}

\subsection{Shear tests}
\label{sec:shear_test}
\begin{figure}
    \centering
    \includegraphics[width = \linewidth]{figures/shear}
    \caption{\emph{left:} shear testing setup \emph{right:} stress-strain curves for a square sample oriented at 0° and 90°, and $\alpha_3$ as a linear approximation of the average of both curves.}
    \label{fig:shear-tests}
\end{figure}
The last coefficient needed for the membrane parametric model is $\alpha_3$ which corresponds to the shear modulus. To measure this coefficient we clamp two parallel edges of a square sample and move one of its edges laterally while probing the response (Fig.~\ref{fig:shear-tests}, left). We performed the measurement twice on the same sample according to two different orientations, \ang{90} from each other. The results at \ang{0} and \ang{90} show an approximately linear response which corresponds well with the choice of expressing shear stress as a function of shear strain in our parametric stress-strain model (\ref{eq:stress_strain_formula}). %$\sigma_{12}$ as a linear function of $\varepsilon_{12}$ in the parametric model.
The two shearing experiments correspond to the same shear strain value and, as expected, lead to the same shear stress response in the linear regime, as evidenced by the common slope of the 2 curves at the origin. However, the behaviors of the \ang{0} and \ang{90} samples diverge for larger strain rates. We suspect this to be due, at least in part, to the anisotropy of the material. Our model cannot reproduce this orientation-dependent behavior and requires a single shear modulus $\alpha_3$, that we compute as the average linear regression of each curve.

% The two shearing experiments correspond to the same shear strain value $E_{12}$ and therefore should give the same shear stress $S_{12}$ as a result. However, a surprising experimental result was that the response was not the same at a \ang{0} and \ang{90} orientation (Fig.~\ref{fig:shear-tests}, right), this could either be due to errors in the experimental process or to more complex effects occurring within the fabric which our model cannot reproduce. Another possibility is that the curves are diverging after the small strain regime because of the orthotropy of the material: at large strains, the deformation is not pure shearing anymore but a mix of shearing and stretching, and since the stretching response is direction dependent this could explain the deviation between the curves. Since our model cannot explain this divergence between the two curves, we compute $\alpha_3$ as the average linear regression of each curve. \melina{Is the asymmetry a suprising result? The shear modulus (like Young's modulus) is defined for linear materials, i.e. for small deformations, and you do get the same slope at the origin, which is reassuring. I don't see why the behavior should be the same for large deformations, specially since you have an anisotropic material (third hypothesis)}\melina{I think that, ideally, what we should have done, is measure the shear modulus on pre-streched fabric and use the slope at the origin.}

\subsection{Bending tests} \label{sec:bending-tests}
\begin{figure}
    \centering
    \includegraphics[width = \linewidth]{figures/cantilever}
    \caption{Matching measurements of cantilevered shapes against the ``master curve'' of \citet{Romero2021}.}
    \label{fig:cantilever-tests}
\end{figure}

The last coefficient needed for the fabric's material model is the \emph{flexural coefficient} $k_B$ of the bending energy. This coefficient, for a homogeneous material with Young's modulus $E_f$, thickness $h_f$ and Poisson's ratio $\nu_f$, is usually expressed as \cite{Tamstorf2013}
$$k_B = \frac{E_fh_f^3}{12(1 - {\nu_f}^2)},$$
but we can directly measure it by performing a cantilever test in which a fabric ribbon sample is clamped horizontally and a length $L$ of itself is subjected to gravity. \citet{Romero2021} show the relationship between a unitless \emph{gravito-bending} parameter $\Gamma=\frac{\rho gh_f}{k_B}L^3$ -- where $\rho$ is the mass density and $g$ is the acceleration of gravity -- and the aspect ratio $y/x$ of the bounding rectangle of the cantilevered sample (see Fig.~\ref{fig:cantilever-tests}, inset). This relationship is universal in the sense that the pair $(\Gamma, y/x)$ will always be on a specific curve, called the \emph{master curve}, no matter the material properties of the sample.

To find the value of $k_B$, we therefore measure $y/x$ for different values of $L$ and find the coefficient $k_B$ such that the different points $(\frac{\rho gh_f}{k_B}L^3, y/x)$ are as close as possible to the curve in the least squares sense. We performed the test on two different orientations of the fabric (\ang{0} and \ang{90}) and tested them both front side up and back side up, for a total of 4 different experiments. The results (Fig.~\ref{fig:cantilever-tests}) show a difference in $k_B$ between the \ang{0} and \ang{90} orientation, which is not surprising given the structure of the knitted textile. The difference between bending front side up and back side up was found to be negligible in the \ang{0} case -- meaning the resistance to bending is essentially symmetric in that direction -- but for the \ang{90} case the flexural coefficient is almost 3 times bigger on one side compared to the other, which can be explained by the fact that knitted textiles, in general, do not exhibit mirror symmetry between their front and back sides and therefore can have fairly different responses between bending upwards and downwards. The Discrete Shells model that we use to model bending does not account for these orientation and direction-dependent effects and is only weighted by one flexural coefficient $k_B$. Therefore, in practice, we compute $k_B$ as the mean value between the 4 measured ones.
%  \melina{It should be straight forward to account for the dependence on the side (by using a different value for $k_B$ depending on the sign of $theta$). Why not doing it?} \david{If I do this, the derivatives of the discrete shells energy would be discontinuous (this would create a kink at 0). I'm guessing that to properly acount for such effects we would need to give up on the Kirchhoff-Love assumptions and use a much more complex model accounting for what's going on inside the thickness of the shell.    } \melina{No, derivatives will be equal to 0 at the transition, so C0 continuous. You'll have a discontinuity in the Hessian but this is usually not an issue for forward simulation.}

All the different parameters used in the bilayer model ($E_f$, $E_p$, $\nu_b$), the membrane energy ($\alpha_1$, $\alpha_2$, $\alpha_3$, $\beta_1$, $\beta_2$, $\gamma_1$, $\gamma_2$, $\nu_f$) and the bending energy ($k_B$) are summed up in table~\ref{tab:parameters}.
% \melina{Check formulas. Unit for $k_B$ seems wrong. Should be N.m to be coherent with the $W_{bending}$ formula provided above. But this would not be coherent with the $k_B$ formula (however, $k_B$ usually depends on $h^3$, not $h^2$. Where does this formula come from?) and would not make $\Gamma$ adimensional...}

\begin{table*}
    \centering
    \begin{tabular}{ccccccccccc}
        \toprule
        $E_1$ (MPa) & $E_2$ (MPa) & $\nu_f$ & $\alpha_1$ &$\alpha_2$ & $\alpha_3$ & $\beta_1$ & $\beta_2$ & $\gamma_1$ & $\gamma_2$ & $k_B$(N.m) \\
        \midrule
        72.3 & 1.05 & 0.3 & 24161 & 90890 & 63419 & 29676 & 20512 & 0.9626 & 0.9637 & $9.57 \times 10^{-6}$ \\
        \bottomrule
    \end{tabular}
    \caption{Parameters measured on a TPU95A filament and a spandex textile stretched up to 70\% of its initial length.}
    \label{tab:parameters}
\end{table*}
