\section{Conclusion}
We have presented a physical model and calibration procedure dedicated to printing-on-fabric. By relying on a data-driven non-linear model for fabric, and on a bilayer shell for the plastic parts, our simulator better reproduces real-world fabricated artifacts than previous rod-based models. In particular, our model captures both the extrinsic curvature produced by plastic strips that bend as circular arcs, and the intrinsic curvature produced by the fabric that contracts non-uniformly depending on plastic density (Fig.~\ref{fig:torus_comparison}).

We tested our model on patterns of locally-parallel strips, as proposed by \citet{Jourdan2022}. Yet, our model is generic and should also work for other patterns demonstrated in prior work, such as tilings of 3-pointed stars \cite{Jourdan2020}, curve networks \cite{Perez2017}, or tilings of hexagons \cite{Fields2018}. An exciting direction for future work would be to integrate our simulator within an inverse-design algorithm, for instance to optimize the layout and thickness of plastic elements to best reproduce a target shape \cite{Jourdan2022}. Indeed, accurate simulation has been critical for inverse design of other types of deployable structures, such as grid shells \cite{Panetta2019} and inflatables \cite{Panetta2021}.

Our formulation and calibration protocol should also apply to other types of fabric, plastic, and amount of stretch. Our model might also be extended to capture other phenomena, such as plasticity of the material. While \citet{Jourdan2020} accounted for plasticity to reproduce the curvature of plastic rods, we did not find this effect necessary in our experiments.
In addition, the bilayer model we rely on assumes isotropic plastic and fabric materials. It could be extended to account for the anisotropy of the fabric and further increase accuracy.

%the anisotropy of the textile into the bilayer model (right now it only works for an isotropic material, that's why we average the Poisson's ratios and Young's moduli over all orientations

Finally, printing-on-fabric is a low-cost fabrication process that exhibits significant imprecision.
In particular, it is common that the plastic extruded by the 3D printer does not adhere well to fabric due to under-extrusion, or leaks from one printed element to the next due to over-extrusion as the printing head moves over the surface. Such imprecision could be modeled explicitly by using a thermo-mechanical simulation of the plastic extrusion process, or implicitly by computing statistics about the frequency of various defects, and by propagating such uncertainty within the simulation of the fabric-plastic assembly.


\begin{acks}
This work was supported by the European Research Council (ERC) starting grants D$^3$ (ERC-2016-STG 714221), GEM  (StG-2014-639139), by the US National Science Foundation (IIS-1910274), and research and software donations from Adobe Inc.

We thank the developers of the open source library Polyscope \citep{polyscope} that we used for rendering the simulations. The fabricated models were photographed by Emilie Yu and were first published in \citep{Jourdan2022}.
\end{acks}

% \david{
%   Future work:
% \begin{itemize}
%   \item Accomodate varying amounts of pre-stretch
%   \item Integrate the anisotropy of the textile into the bilayer model (right now it only works for an isotropic material, that's why we average the Poisson's ratios and Young's moduli over all orientations). 
% \end{itemize}}
% \adrien{I briefly mentioned both items in the 3rd paragraph}


%The goal of this paper was twofold: better understanding, by means of physical experiments, the mechanics of pre-stretched textiles when released and their interaction with thermoplastics printed on top, and deriving a simulation model from those measurements and experiments which should be both accurate and efficient. 

%At the moment, this method is still very much work in progress and needs to be validated against a variety of printed patterns to assess its accuracy. To check that the bending behavior caused by the bilayer effect is modeled correctly, we will compare the curvature of simulations of parallel ribbon patterns such as the one shown in Fig.~\ref{fig:cylinder-result} with the measured curvature of their fabricated counterparts. To check that the intrinsic contraction behaves similarly to printed patterns, we can print a pattern which reproduces half a torus by having a lower density of curves on the inside of the flattened shape and a higher density of curves on the outside (as in \citep{Jourdan2022}). After performing these validations we will try to simulate our own printed patterns and a variety of other curve layouts inspired from previous work (such as the self-shaping textiles shown in introduction).

%However, the printed plastic material might exhibit a certain amount of plasticity \citep{Jourdan2020} which has not yet been taken into account in a systematic way in the model, and therefore might cause the simulated results to be stiffer than the printed ones, this could prevent the method to accurately reproduce extrinsic curvature in particular. Another issue with the method is the amount of experimental data it requires to find the parameters for given plastic and fabric materials, it would be useful to have a way to easily calibrate the simulation method to other unseen materials (provided they are not too different). One way to do this would be to propose a set of tests that would each be used to measure a specific parameter. For example, measuring the curvature of an individual bilayer ribbon can be used to derive the Young's moduli ratio $E_1/E_2$ or measuring a series of parallel ribbons at different orientations gives information about the orthotropic fabric response. 