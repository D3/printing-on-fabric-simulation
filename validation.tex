\section{Validation}
We now describe several numerical experiments comparing the results of our simulator against real-world fabricated samples.
We order the experiments by complexity, starting with empty fabric, then moving to the simulation of a single plastic strip, all the way to the simulation of doubly-curved surfaces.

\paragraph{Fabric model.}
Fig.~\ref{fig:elongation-validation} plots the forces integrated along each edge of a rectangular fabric sample undergoing uniaxial stretch, which reproduces the experiment shown in Fig.~\ref{fig:uniaxial-setup}.
% \adrien{did I get this right?}\david{perfect}
Our simulator yields different forces depending on the orientation of the fabric sample, which matches the measurements well. %\adrien{Should we keep this experiment? It seems to be more of a sanity check since we fitted the model to reproduce these measurements, no?}

\begin{figure}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/elongation}
    \caption{Comparison between simulated force values and measurements for different orientations of a fabric sample undergoing uniaxial strain.}
    \label{fig:elongation-validation}
\end{figure}

\begin{wrapfigure}[5]{r}{0.2\textwidth}
\vspace{-0.18in}
\includegraphics[width=\linewidth]{figures/single_strip}
%\vspace{-0.3in}
\end{wrapfigure}
\paragraph{Single plastic strip.}
We first evaluate our simulator on a simple assembly composed of a single plastic strip printed on pre-stretched fabric, which curves to form a circular arc when released, as shown as inset. We keep the length and width of the strip fixed and vary its thickness. As shown in Fig.~\ref{fig:curvature-validation}, our simulator predicts that the curvature of the arc decreases as plastic thickness increases, and the curvature magnitude matches measurements on real samples. Note that the real-world measurements exhibit variance, especially at low thickness due to printing imprecision.

\begin{figure}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/curvature-comparison}
    \caption{Evolution of the curvature of a plastic strip printed on fabric as a function of plastic thickness. Error bars were computed over 5 measured samples.}
    \label{fig:curvature-validation}
\end{figure}

\paragraph{Multiple parallel plastic strips.}
We next evaluate the ability of our simulator to account for the interactions between fabric and plastic in the presence of multiple strips. We focus on the same pattern of staggered parallel strips as used by \citet{Jourdan2022}, which yield cylindrical surfaces as the assembly rolls on itself when released (Fig.~\ref{fig:cylinder-experiment}a,b). We keep the length and width of the strips fixed and vary their thickness $h_p$ and spacing $\mu$.
Fig.~\ref{fig:cylinder-validation} plots the average curvature of the strips produced by our simulator against the curvature of real samples, which we measured by fitting a circle onto front-view pictures of the cylinders (Fig.~\ref{fig:cylinder-experiment}c). We measured the curvature of samples printed on the front side as well as on the back side of the fabric, as we observed that fabric side impacts curvature, possibly because a different quantity of plastic is deposited depending on the roughness of the fabric surface (Fig.~\ref{fig:textile-asymmetry}). Overall, our simulator reproduces well the curvature of real samples. First, curvature decreases as plastic thickness increases. Second, curvature increases as spacing between the strips increases, because more fabric exerts forces on each strip.

\begin{figure*}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/cylinder_experiment}
    \caption{We focused our evaluation on a pattern of parallel plastic strips (a), which rolls as a cylinder when released. Our simulation reproduces well this behavior (b). We fabricated multiple such cylinders by varying the thickness $h_p$ and spacing $\mu$ of the strips (c), and measured their curvature by fitting a circle on their silhouette (c, orange circle).}
    \label{fig:cylinder-experiment}
\end{figure*}

\begin{figure*}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/cylinder-comparison}
    \caption{Evolution of the curvature of parallel plastic strips printed on fabric as a function of plastic thickness $h_p$ and strip spacing $\mu$. Our simulator reproduces well the variations of curvature measured on real samples.}
    \label{fig:cylinder-validation}
\end{figure*}

\begin{figure}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/textile-asymmetry}
    \caption{We hypothesize that fabric has a different roughness on each side. As a consequence, a different quantity of plastic is deposited, which would explain why we measured a different curvature depending on printing side (Fig.~\ref{fig:cylinder-validation}). }
    \label{fig:textile-asymmetry}
\end{figure}

\paragraph{Complex strip pattern.}
Finally, we evaluate our simulator on a more complex assembly of radially-oriented strips with varying spacing, which should shape as a torus on release. Fig.~\ref{fig:torus_comparison} shows that our simulator effectively achieves a toric shape, while the simulator of \citet{Jourdan2020} under-estimates the curvature of the torus as it does not account for how the fabric contracts by a different amount transversely to the strips depending on strip density. While we used a procedural radial pattern for this experiment, Figure~\ref{fig:teaser} demonstrates successful simulation of a pattern computed with the inverse-design method of \citet{Jourdan2022}, achieving a good prediction of the shape obtained when printing the same pattern.  Figure~\ref{fig:gallery} provides additional visual comparisons between our simulations and fabricated assemblies obtained with patterns from \citet{Jourdan2022}.

\begin{figure*}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/torus_comparison}
    \caption{Existing rod-based models fail to capture the intrinsic curvature induced by varying density of plastic strips over the surface. Starting with a procedural radial pattern of spatially-varying density (a), the simulator by \citet{Jourdan2020} contracts the pattern uniformly and only reproduces extrinsic curvature along the strips (b). In contrast, by modeling the plastic-fabric bilayer with a surface-based model, our approach captures well the non-uniform contraction transversely to the strip pattern, yielding the expected toric shape as the pattern contracts more along the inner boundary than along the outer one.}
    \label{fig:torus_comparison}
\end{figure*}

\begin{figure*}[h!]
    \centering
    \includegraphics[width = \linewidth]{figures/gallery}
    \caption{Visual comparison between the results of our simulator (b) and fabricated assemblies (c), using patterns computed with the inverse-design method of \citet{Jourdan2022} (a). The Skirt pattern (bottom row) contains strips printed on the back side of the fabric to produce negative extrinsic curvature.}
    \label{fig:gallery}
\end{figure*}